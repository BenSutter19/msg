/*
  Copyright (C) 2016 Ben Sutter

  This file is the client-side class file in MSG.
  
  MSG is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  MSG is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with MSG.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include "gettext.h"
#include "config.h"
#include <locale.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <readline/readline.h>
#include <getopt.h>
#include <readline/history.h>
#include <arpa/inet.h>
#include <headers.h>

#define _(String) gettext (String)

// returns 0 if successful, 1 for error, 2 for unsuccessful connection
int
client (char *username, char *addr, char *port)
{
  int list_socket, numbytes; // numbytes is the number of bytes received
    char msg[600];
    char *line;
    char user_line[600];
    struct addrinfo hints, *servinfo, *p;
    int rv;
    char s[INET6_ADDRSTRLEN];

    memset (&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM; // operating on TCP/IP

    // set up destination port and address
    if ((rv = getaddrinfo (addr, port, &hints, &servinfo)) != 0)
      {
        fprintf (stderr, "msg: getaddrinfo: %s\n", gai_strerror (rv));
        return 1;
      }

    // loop through all the results and connect to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next)
      {
        if ((list_socket = socket (p->ai_family, p->ai_socktype,
				   p->ai_protocol)) == -1)
	  {
	    perror ("msg: socket");
	    continue;
	  }

        if (connect (list_socket, p->ai_addr, p->ai_addrlen) == -1)
	  {
            close (list_socket);
            perror ("msg: connect");
            continue;
	  }
	
        break;
      }

    if (p == NULL)
      {
        fprintf (stderr, "msg: failed to connect\n");
        return 2;
      }

    inet_ntop (p->ai_family, get_in_addr ((struct sockaddr *)p->ai_addr),
            s, sizeof s);
    printf ("msg: connecting to %s\n", s);

    freeaddrinfo (servinfo); // all done with this structure

    while (1)
      {
	line = readline ("msg> ");
	if (line)
	  {
	    if (strcmp (line, "/quit") == 0) // quit command closes sockets and application
		break;
	    add_history (line);
	    snprintf (user_line, sizeof user_line, "%s: %s\n", username, line);
	    if (send (list_socket, user_line, sizeof user_line, 0) == -1)
	      perror ("msg: send");
	  }
	    if ((numbytes = recv (list_socket, msg, sizeof msg, 0)) == -1)
	      perror ("msg: recv");
	    if (msg)
	      {
		msg[numbytes] = '\0';
		printf ("%s", msg);
	      }
      }
    
    close (list_socket);
    return 0;
}
