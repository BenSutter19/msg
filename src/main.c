/*
  MSG - Messaging System for Groups is a chatroom-based, self-hosted, decentralized instant messenger.

  Copyright (C) 2016 Ben Sutter

  This file is the main class file in MSG.
  
  MSG is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  MSG is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with MSG.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include "gettext.h"
#include "config.h"
#include <locale.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <getopt.h>

#define _(String) gettext (String)

static char *username = _("Messenger"); // default user name
static char *addr = "127.0.0.1"; // default target (for client) or home (for server) IPv4 address
static char *port = "8824"; // default port number (either open this one or use on that is open, if you want to connect from/to other networks)
static int client_flag = 0, version_flag = 0, server_flag = 0, help_flag = 0;

static struct option long_options[] =
  {
    {"help", no_argument, &help_flag, 1},
    {"version", no_argument, &version_flag, 1},
    {"server", no_argument, &server_flag, 1},
    {"client", no_argument, &client_flag, 1},
    {"username", required_argument, 0, 'u'},
    {"address", required_argument, 0, 'a'},
    {"port", required_argument, 0, 'p'},
    {0, 0, 0, 0}
  };  

int
parse_opts (int argc, char *argv[])
{
  int c;
  int option_index = 0;
  
  while ((c = getopt_long (argc, argv, "u:a:p:", long_options, &option_index)) != -1)
    {
      switch(c)
	{
	case 'u':
	  username = optarg;
	  break;
	case 'a':
	  addr = optarg;
	  break;
	case 'p':
	  port = optarg;
	  break;
	}
    }
  return 0;
}
  
int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");
  bindtextdomain (PACKAGE, LOCALEDIR);
  textdomain (PACKAGE);

  parse_opts(argc, argv);

  if (version_flag == 1) {
    help_flag = 0;
    server_flag = 0;
    client_flag = 0;
    version_flag = 0;
    printf ("%s\nCopyright (C) 2016 Ben Sutter\nLicense GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>\nThis is free software: you are free to change and redistribute it.\nThere is NO WARRANTY, to the extent permitted by law.\n", PACKAGE_STRING);
  }

  if (help_flag == 1) {
    help_flag = 0;
    server_flag = 0;
    client_flag = 0;
    version_flag = 0;
    // TODO: add all options to help message
    printf ("For the most basic client purposes, msg -client will do.\nPlease consult the manual for further instruction.\nGeneral help using GNU software: <http://www.gnu.org/gethelp/>\n");
  }

  if (server_flag == 1) {
    help_flag = 0;
    server_flag = 0;
    client_flag = 0;
    version_flag = 0;
    printf ("msg: server command invoked, passing control to the server\n");
    if (server (port) != 0)
      {
	printf ("msg: server failed to start\n");
	exit (1);
      }
  }

  if (client_flag == 1)
    {
      help_flag = 0;
      server_flag = 0;
      client_flag = 0;
      version_flag = 0;
      printf ("msg: client command invoked, passing control to the client\n");
      if (client (username, addr, port) != 0)
	{
	  printf ("msg: client failed to start\n");
	  exit (1);
	}
    }

  return 0;
}
