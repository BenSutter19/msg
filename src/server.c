/*
  Copyright (C) 2016 Ben Sutter

  This file is the server-side class file in MSG.
  
  MSG is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  MSG is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with MSG.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pthread.h>
#include <arpa/inet.h>
#include <stdio.h>
#include "gettext.h"
#include "config.h"
#include <locale.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <readline/readline.h>
#include <getopt.h>
#include <readline/history.h>

#define _(String) gettext (String)

void
*get_in_addr (struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void
*socket_thread (int con_socket)
{
  char msg[600];
  int numbytes;
  while (1)
    {
      numbytes = recv (con_socket, msg, sizeof msg, 0);
      if (numbytes == -1)
	perror ("msg: recv");
      if (numbytes == 0)
	break;
      if (msg) {
	msg[numbytes] == '\0';
	printf ("%s", msg);
	if (send (con_socket, msg, sizeof msg, 0) == -1)
	  perror ("msg: send");
      }
    }
  return 0;
}

// returns 0 if successful, 1 otherwise
int
server (char *port)
{
  int my_socket, con_socket;  // listen on my_socket, new connection on con_socket
  struct addrinfo hints, *servinfo, *p;
  struct sockaddr_storage con_addr; // connector's address information
  socklen_t sin_size;
  int yes = 1;
  char s[INET6_ADDRSTRLEN];
  int rv;
  char *line;
  
  memset (&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE; // use my IP
  
  if ((rv = getaddrinfo (NULL, port, &hints, &servinfo)) != 0)
    {
      fprintf (stderr, "msg: getaddrinfo: %s\n", gai_strerror (rv));
      return 1;
    }
  
  // loop through all the results and bind to the first we can
  for(p = servinfo; p != NULL; p = p->ai_next)
    {
      if ((my_socket = socket (p->ai_family, p->ai_socktype,
			       p->ai_protocol)) == -1) {
	perror("msg: socket");
	continue;
      }
      
      if (setsockopt (my_socket, SOL_SOCKET, SO_REUSEADDR, &yes,
		      sizeof(int)) == -1)
	{
	  perror("msg: setsockopt");
	  exit(1);
	}
      
      if (bind (my_socket, p->ai_addr, p->ai_addrlen) == -1)
	{
	  close (my_socket);
	  perror("msg: bind");
	  continue;
	}
      break;
    }
  
  freeaddrinfo(servinfo); // all done with this structure
  
  if (p == NULL)
    {
      fprintf (stderr, "msg: failed to bind\n");
      exit(1);
    }
  
  if (listen (my_socket, 10) == -1)
    {
      perror("msg: listen");
      exit(1);
    }
  
  printf("msg: waiting for connections...\n");
  
  while(1) // main accept() loop
    {      
      sin_size = sizeof con_addr;
      con_socket = accept (my_socket, (struct sockaddr *)&con_addr, &sin_size);
      if (con_socket == -1)
	{
	  perror ("msg: accept");
	}
      
      inet_ntop(con_addr.ss_family,
		get_in_addr((struct sockaddr *)&con_addr),
		s, sizeof s);
      printf("server: got connection from %s\n", s);

      pthread_t *ptid;
      if (pthread_create (ptid, NULL, socket_thread, (void *)con_socket) < 0)
	perror ("msg: pthread_create");
    }
  
  return 0;
}
